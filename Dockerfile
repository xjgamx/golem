FROM c0mradeuc/node-alpine-consul:stable

WORKDIR /home/app
COPY package.json package-lock.json ./

RUN npm install

COPY . ./
EXPOSE 3000
CMD ["/bin/sh", "./consul/entrypoint.sh"]
