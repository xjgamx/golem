#!/bin/sh

set -e

echo "Validating required params..."

if [ -z $ENVIRONMENT ]; then
  echo "ENVIRONMENT is missing"
  exit 1
fi

if [ -z $CONSUL_HTTP_ADDR ]; then
  echo "CONSUL_HTTP_ADDR is missing"
  exit 2
fi

if [ -z $CONSUL_HTTP_TOKEN ]; then
  echo "CONSUL_HTTP_TOKEN is missing"
  exit 3
fi

echo "Getting configuration from consul and BOOT..."
echo "  ENVIRONMENT: $ENVIRONMENT"
echo "  Consul Url:  $CONSUL_HTTP_ADDR"

/consul-template -config ./consul/config.hcl -once -log-level debug

if [ -z $CONSUL_DEBUG ]; then
  echo "  CONSUL_DEBUG is disabled"
else
  echo "renderer template: "
  cat ./config.json
fi

npm start
