/*jshint esversion: 6 */
const Constant = require('./../../config.json');
const routes = require('express').Router();
const authController = require('../Controllers/auth0');
const Services = require('./../Services');

routes.post(Constant.endPoint.login, (req, res) => {
  if (!req.body.username || !req.body.password) {
    res.json({
      error: true,
      code: 502,
      message: 'Los campos username y password son requeridos'
    });
  } else {
    const params = { username: req.body.username, password: req.body.password };
    saveReqtoDB(req, res, params);
    
    authController.authUser(params, (err, response, userIsValid = false) => {
      if (err) {
        res.json({
          error: true,
          code: 502,
          message: err
        });
      } else {
        res.send({ 'aboutUser': userIsValid, 'body': response });
      }
    });
  }
});
routes.post(Constant.endPoint.infoUser, (req, res) => {
  if (!req.body.username) {
    res.json({
      error: true,
      code: 502,
      message: 'El campo username es requerido'
    });
  } else {
    const params = { username: req.body.username };
    saveReqtoDB(req, res, params);
    
    authController.authAPP(params, (err, response, userExist = false) => {
      if (err) {
        res.json({
          error: true,
          code: 502,
          message: err
        });
      } else {
        res.send({ 'aboutUser': userExist, 'body': response });
      }
    });
  }
});

const saveReqtoDB = function (req, res, data) {

  const dataRequest = {
    url: req.url,
    method: req.method,
    data: data,
    headers: req.headers
  };

   Services.logAccessServices.createLog(dataRequest, (err, res) => {
    if (err) throw new Error(err);
    console.info('Estos datos fueron guardados a la tabla logAccess', dataRequest);
  });
};

module.exports = routes;