'use strict';

const Models = require('../Models')

const createLog = function (objToSave, callback) {
    new Models.logAccess(objToSave).save( callback);
}

module.exports = {
    createLog
}