/*jshint esversion: 6 */
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const routes = require('./Routes');
const Constant = require('./../config.json');
const mongoose = require("mongoose");

const PORT = process.env.PORT || 3000;

mongoose.set( 'useNewUrlParser', true );
mongoose.set( 'useCreateIndex', true );
mongoose.connect(Constant.dbURI, (error, connection) => {
  if (error) {
    console.error('Mongo Not Conected', error);
    process.exit(1);
  } else {
    console.info('Conected DataBase, Connection:', connection);
  }
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/* Conectar todas las rutas de la app */
app.use('/', routes);

/* Logica de inicio de servidor, en este caso solo se levanta el servidor en el puerto 3000 */
app.listen(PORT, function () {
  console.info(`El endpoint corre en localhost:${Constant.PORT}${Constant.endPointLogin}`);
});
